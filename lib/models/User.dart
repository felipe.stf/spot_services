class User {
  String about;
  String name;
  String email;
  String picture;
  int age;
  String city;
  String phone;

  User(this.about, this.name, this.email, this.picture, this.age, this.city, this.phone);

  User.fromJson(Map<String, dynamic> json) {
    about = json['about'];
    name = json['name'];
    email = json['email'];
    picture = json['picture'];
    age = json['age'];
    city = json['city'];
    phone = json['phone'];
  }
}