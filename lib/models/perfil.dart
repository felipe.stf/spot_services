import 'package:firebase_database/firebase_database.dart';

class Perfil {
  String key;
  String email;
  String name;
  String born;
  String cpfcnpj;
  String city;
  String age;

  Perfil( this.email, this.name, this.born, this.cpfcnpj, this.city, this.age);

  Perfil.fromSnapshot(DataSnapshot snapshot) :
    key = snapshot.key,
    email = snapshot.value["email"],
    name = snapshot.value["name"],
    born = snapshot.value["born"],
    cpfcnpj = snapshot.value["cpfcnpj"],
    city = snapshot.value["city"],
    age = snapshot.value["age"];

  toJson() {
    return {
      "email": email,
      "name": name,
      "born": born,
      "cpfcnpj": cpfcnpj,
      "city": city,
      "age": age,
    };
  }
}