import 'package:flutter/material.dart';
import 'package:spot_service/services/authentication.dart';
import 'dart:async';

class HelpPage extends StatefulWidget {
  HelpPage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _HelpPageState();
}

class _HelpPageState extends State<HelpPage> {
 
  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }



  Widget showTodoList() {    
      return Container(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: Column(
          children: <Widget>[
             Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              color: Colors.white, 
              child: Container(
                padding: EdgeInsets.all(32.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text('SOBRE',
                    textAlign: TextAlign.left,
                     style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Color.fromRGBO(68, 68, 68, 1)  ),
                    ),
                    Container(
                    height: 30,
                    ),
                    Text('Sobre a Spot Service',
                    style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1))
                    ),
                    Container(
                    alignment:  Alignment(1.0, 1.0),
                    height: 30,
                    child: Icon(
                      Icons.verified_user ,
                      color: Colors.blue,
                      size: 30.0,
                    ),
                    ),
                    Container(
                    width:230 ,
                    child: Text('A Spot Service nasceu em uma matéria da faculdade criada por 5 programadores que viram uma necessidade de mercado e resolveram abraçar',
                    style: TextStyle(fontSize: 20, color: Color.fromRGBO(0, 0, 0, 0.6))
                    ),
                    ),
                    
                  ],
                ),
              ),
            ),
            Container(
            height: 30,
            ),
            Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(20.0),
              ),
              color: Colors.white, 
              child: Container(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                    width: 155555,
                    ),
                    Text('Fale conosco',
                    textAlign: TextAlign.left,
                     style: TextStyle(fontSize: 25, fontWeight: FontWeight.bold, color: Color.fromRGBO(68, 68, 68, 1)  ),
                    ),
                    Container(
                    alignment:  Alignment(1.0, 1.0),
                    height: 30,
                    child: Icon(
                      Icons.verified_user ,
                      color: Colors.blue,
                      size: 30.0,
                    ),
                    ),
                    
                    Text('spot_services@contato.com.br',
                    style: TextStyle(fontSize: 15, color: Color.fromRGBO(0, 0, 0, 0.6)),),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
     );
  
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Spot service'),
          centerTitle: true ,
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: signOut)
          ],
        ),
        body: showTodoList(),
        );
  }
}
