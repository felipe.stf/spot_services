import 'package:flutter/material.dart';
import 'package:spot_service/pages/profile_edit.dart';
import 'package:spot_service/services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:spot_service/models/perfil.dart';
import 'dart:async';
import 'chat.dart';
import 'interesse_page.dart';

class ProfilePage extends StatefulWidget {
  ProfilePage({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  List<Perfil> _todoList;
  String _email;

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  StreamSubscription<Event> _onTodoAddedSubscription;
  Query _todoQuery;



  @override
  void initState() {
    super.initState();
    _todoList = new List();
    widget.auth.getCurrentUser().then((user) {
        _email = user.email;
        _todoQuery = _database
        .reference()
        .child("perfil")
        .orderByChild("email")
        .equalTo(_email);
    _onTodoAddedSubscription = _todoQuery.onChildAdded.listen(onEntryAdded);
    });
    //_checkEmailVerification();
    }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }


  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    super.dispose();
  }

  onEntryAdded(Event event) {
    setState(() {
      _todoList.add(Perfil.fromSnapshot(event.snapshot));
    });
  }

  Widget showProfile() {   
      return Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
      appBar: AppBar(
        title: Text(' '),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Column(
      children: <Widget>[
      Container(
        width: MediaQuery.of(context).size.width,
        height: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(64),
            bottomLeft: Radius.circular(64)
          )
        ),
        child: Column(
          children: <Widget>[
            Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text('Idade',
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(_todoList[0].age,
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                      Container(
                        margin: const EdgeInsets.only(left: 90),
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage('https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png')
                          )
                        ),
                      ),
                    Padding(
                      padding: const EdgeInsets.only(right: 0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 65),
                            child: Text('Cidade',
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 65),
                            child: Text(_todoList[0].city,
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 18),
                child: Text(_todoList[0].name,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  )
                ),
              )
          ],
        )
      ),
      Container(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: Column(
          children: <Widget>[
             GestureDetector( // um widget normal
                onTap: () { // uma das propriedades do [GestureDetector]
                //esta função será chamada quando o filho(child) do widget for pressionado
                  Navigator.push(context,
                  MaterialPageRoute(builder: (context) => EditProfile(auth: widget.auth, logoutCallback: widget.logoutCallback,)),);
                },
                child:Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    color: Colors.white, 
                    child: Container( //o container vai representar o nosso botão
                    width: 340.0,
                    height: 70.0,
                    alignment:  FractionalOffset(0.1, 0.5),
                    child: Text('Informações pessoais',
                            style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                            ),
                ))),
                Container(
                  height: 3,
                ),
                GestureDetector( // um widget normal
                  onTap: () { // uma das propriedades do [GestureDetector]
                  //esta função será chamada quando o filho(child) do widget for pressionado
                    //var data = [{'index':0,'name':"Eletricista",'check':true},{'index':1,'name':"Marcenaria",'check':false},{'index':2,'name':"Encanador",'check':false},{'index':3,'name':"Mecânico",'check':false}];
                    Navigator.push(context,
                    MaterialPageRoute(builder: (context) => InteressePage()),);
                  },
                  child:Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      color: Colors.white, 
                      child: Container( //o container vai representar o nosso botão
                      width: 340.0,
                      height: 70.0,
                      alignment:  FractionalOffset(0.1, 0.5),
                      child: Text('Interesses',
                              style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                              ),
                  ))),
                  Container(
                    height: 3,
                  ),
                  GestureDetector( // um widget normal
                  onTap: () { // uma das propriedades do [GestureDetector]
                  //esta função será chamada quando o filho(child) do widget for pressionado
                    
                  },
                  child:Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      color: Colors.white, 
                      child: Container( //o container vai representar o nosso botão
                      width: 340.0,
                      height: 70.0,
                      alignment:  FractionalOffset(0.1, 0.5),
                      child: Text('Contato',
                              style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                              ),
                  ))),
          ],
        ),
      ),
     )]));
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Spot service'),
          centerTitle: true ,
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: signOut)
          ],
        ),
        body: showProfile(),
        );
  }
}