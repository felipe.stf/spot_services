import 'package:flutter/material.dart';
import 'package:spot_service/pages/profile_page.dart';
import 'package:spot_service/pages/sp_list_page.dart';
import 'package:spot_service/services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:spot_service/models/todo.dart';
import 'package:spot_service/pages/help_page.dart';
import 'dart:async';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.auth, this.email, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;
  final String email;

  @override
  State<StatefulWidget> createState() => new _HomePageState();
}

class _HomePageState extends State<HomePage> {
  List<Todo> _todoList;

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();

  final _textEditingController = TextEditingController();
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;

  Query _todoQuery;

  //bool _isEmailVerified = false;

  @override
  void initState() {
    super.initState();

    //_checkEmailVerification();

    _todoList = new List();
    _todoQuery = _database
        .reference()
        .child("todo")
        .orderByChild("userId")
        .equalTo(widget.userId);
    _onTodoAddedSubscription = _todoQuery.onChildAdded.listen(onEntryAdded);
    _onTodoChangedSubscription =
        _todoQuery.onChildChanged.listen(onEntryChanged);
  }

//  void _checkEmailVerification() async {
//    _isEmailVerified = await widget.auth.isEmailVerified();
//    if (!_isEmailVerified) {
//      _showVerifyEmailDialog();
//    }
//  }

//  void _resentVerifyEmail(){
//    widget.auth.sendEmailVerification();
//    _showVerifyEmailSentDialog();
//  }

//  void _showVerifyEmailDialog() {
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        // return object of type Dialog
//        return AlertDialog(
//          title: new Text("Verify your account"),
//          content: new Text("Please verify account in the link sent to email"),
//          actions: <Widget>[
//            new FlatButton(
//              child: new Text("Resent link"),
//              onPressed: () {
//                Navigator.of(context).pop();
//                _resentVerifyEmail();
//              },
//            ),
//            new FlatButton(
//              child: new Text("Dismiss"),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }

//  void _showVerifyEmailSentDialog() {
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        // return object of type Dialog
//        return AlertDialog(
//          title: new Text("Verify your account"),
//          content: new Text("Link to verify account has been sent to your email"),
//          actions: <Widget>[
//            new FlatButton(
//              child: new Text("Dismiss"),
//              onPressed: () {
//                Navigator.of(context).pop();
//              },
//            ),
//          ],
//        );
//      },
//    );
//  }

  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    _onTodoChangedSubscription.cancel();
    super.dispose();
  }

  onEntryChanged(Event event) {
    var oldEntry = _todoList.singleWhere((entry) {
      return entry.key == event.snapshot.key;
    });

    setState(() {
      _todoList[_todoList.indexOf(oldEntry)] =
          Todo.fromSnapshot(event.snapshot);
    });
  }

  onEntryAdded(Event event) {
    setState(() {
      _todoList.add(Todo.fromSnapshot(event.snapshot));
    });
  }

  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
    } catch (e) {
      print(e);
    }
  }

  addNewTodo(String todoItem) {
    if (todoItem.length > 0) {
      Todo todo = new Todo(todoItem.toString(), widget.userId, false);
      _database.reference().child("todo").push().set(todo.toJson());
    }
  }

  updateTodo(Todo todo) {
    //Toggle completed
    todo.completed = !todo.completed;
    if (todo != null) {
      _database.reference().child("todo").child(todo.key).set(todo.toJson());
    }
  }

  deleteTodo(String todoId, int index) {
    _database.reference().child("todo").child(todoId).remove().then((_) {
      print("Delete $todoId successful");
      setState(() {
        _todoList.removeAt(index);
      });
    });
  }

  showAddTodoDialog(BuildContext context) async {
    _textEditingController.clear();
    await showDialog<String>(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            content: new Row(
              children: <Widget>[
                new Expanded(
                    child: new TextField(
                  controller: _textEditingController,
                  autofocus: true,
                  decoration: new InputDecoration(
                    labelText: 'Add new todo',
                  ),
                ))
              ],
            ),
            actions: <Widget>[
              new FlatButton(
                  child: const Text('Cancel'),
                  onPressed: () {
                    Navigator.pop(context);
                  }),
              new FlatButton(
                  child: const Text('Save'),
                  onPressed: () {
                    addNewTodo(_textEditingController.text.toString());
                    Navigator.pop(context);
                  })
            ],
          );
        });
  }

  Widget showTodoList() {
    if (_todoList.length > 0) {
      return ListView.builder(
          shrinkWrap: true,
          itemCount: _todoList.length,
          itemBuilder: (BuildContext context, int index) {
            String todoId = _todoList[index].key;
            String subject = _todoList[index].subject;
            bool completed = _todoList[index].completed;
            String userId = _todoList[index].userId;
            return Dismissible(
              key: Key(todoId),
              background: Container(color: Colors.red),
              onDismissed: (direction) async {
                deleteTodo(todoId, index);
              },
              child: ListTile(
                title: Text(
                  subject,
                  style: TextStyle(fontSize: 20.0),
                ),
                trailing: IconButton(
                    icon: (completed)
                        ? Icon(
                            Icons.done_outline,
                            color: Colors.green,
                            size: 20.0,
                          )
                        : Icon(Icons.done, color: Colors.grey, size: 20.0),
                    onPressed: () {
                      updateTodo(_todoList[index]);
                    }),
              ),
            );
          });
    } else {
      return Container(
        child:Column(
          children:<Widget>[
          Center(
            child: Padding(
              padding: const EdgeInsets.all(16),
              child: Text(
          "Menu principal",
          textAlign: TextAlign.center,
          style: TextStyle(fontSize: 30.0),
        ),
            )),
        
        GestureDetector( // um widget normal
          onTap: () { // uma das propriedades do [GestureDetector]
          //esta função será chamada quando o filho(child) do widget for pressionado
            Navigator.push(context,
            MaterialPageRoute(builder: (context) => HelpPage()),);
          },
          child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
              color: Colors.white, 
              child: Container( //o container vai representar o nosso botão
              width: 380.0,
              height: 80.0,
              alignment:  Alignment(0, 0),
              child: Text('TELA DE AJUDA',
                      style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                      ),
          ))),
        GestureDetector( // um widget normal
          onTap: () { // uma das propriedades do [GestureDetector]
          //esta função será chamada quando o filho(child) do widget for pressionado
            Navigator.push(context,
            MaterialPageRoute(builder: (context) => ProfilePage(auth: widget.auth, logoutCallback: widget.logoutCallback,)),);
          },
          child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
              color: Colors.white, 
              child: Container( //o container vai representar o nosso botão
              width: 380.0,
              height: 80.0,
              alignment:  Alignment(0, 0),
              child: Text('PERFIL',
                      style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                      ),
          ))),
        GestureDetector( // um widget normal
          onTap: () { // uma das propriedades do [GestureDetector]
          //esta função será chamada quando o filho(child) do widget for pressionado
            Navigator.push(context,
            MaterialPageRoute(builder: (context) => SpListPage(auth: widget.auth, logoutCallback: widget.logoutCallback,)),);
          },
          child:Card(
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(50.0),
              ),
              color: Colors.white, 
              child: Container( //o container vai representar o nosso botão
              width: 380.0,
              height: 80.0,
              alignment:  Alignment(0, 0),
              child: Text('LISTAGEM DE PRESTADOR DE SERVIÇO',
                      style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                      ),
          ))),
          ]
        )
        );
    }
  }


  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Spot service'),
          centerTitle: true ,
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: signOut)
          ],
        ),
        body:  showTodoList(),
        // floatingActionButton: FloatingActionButton(
        //   onPressed: () {
        //     showAddTodoDialog(context);
        //   },
        //   tooltip: 'Increment',
        //   child: Icon(Icons.add),
        // )
        );
  }
}