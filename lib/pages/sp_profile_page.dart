import 'package:flutter/material.dart';
import 'package:spot_service/models/User.dart';
import 'package:spot_service/pages/sp_about_page.dart';
import 'package:spot_service/services/authentication.dart';
import 'package:spot_service/pages/sp_info_page.dart';
import 'chat.dart';

class SpProfilePage extends StatefulWidget {
  
  final User user;
  final BaseAuth auth;

  SpProfilePage(this.user, this.auth);

  @override
  State<StatefulWidget> createState() => new _SpProfilePageState();
}

class _SpProfilePageState extends State<SpProfilePage> {
  
  Widget showProfile() {   
      return Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
      appBar: AppBar(
        title: Text(' '),
        elevation: 0,
        backgroundColor: Colors.white,
      ),
      body: Column(
      children: <Widget>[
      Container(
        width: MediaQuery.of(context).size.width,
        height: 200,
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(64),
            bottomLeft: Radius.circular(64)
          )
        ),
        child: Column(
          children: <Widget>[
            Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(left: 0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 20),
                            child: Text('Idade',
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 15),
                            child: Text(this.widget.user.age.toString(),
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 80, right: 30),
                      child: Container(
                        width: 120,
                        height: 120,
                        decoration: BoxDecoration(
                          shape: BoxShape.circle,
                          image: DecorationImage(
                            fit: BoxFit.fill,
                            image: NetworkImage(/*'https://cdn.icon-icons.com/icons2/1378/PNG/512/avatardefault_92824.png'*/ this.widget.user.picture)
                          )
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 0),
                      child: Column(
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.only(left: 0),
                            child: Text('Cidade',
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(left: 0),
                            child: Text(this.widget.user.city,
                              style: TextStyle(
                                color: Colors.black
                              ),
                            ),
                          )
                        ],
                      ),
                    )
                  ],
              ),
              Padding(
                padding: const EdgeInsets.only(top: 18),
                child: Text(this.widget.user.name,
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                  )
                ),
              )
          ],
        )
      ),
      Container(
      padding: EdgeInsets.all(10.0),
      child: Center(
        child: Column(
          children: <Widget>[
            GestureDetector( // um widget normal
                onTap: () { // uma das propriedades do [GestureDetector]
                //esta função será chamada quando o filho(child) do widget for pressionado
                  //Navigator.push(context,
                  //MaterialPageRoute(builder: (context) => HelpPage()),);
                  
                  Navigator.push(context, 
                    new MaterialPageRoute(builder: (context) => SpInfoPage(this.widget.user)));
                },
                child:Card(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20.0),
                    ),
                    color: Colors.white, 
                    child: Container( //o container vai representar o nosso botão
                    width: 340.0,
                    height: 80.0,
                    alignment:  FractionalOffset(0.1, 0.5),
                    child: Text('Informações pessoais',
                            style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                            ),
                ))),
                Container(
                  height: 3,
                ),
                GestureDetector( // um widget normal
                  onTap: () { // uma das propriedades do [GestureDetector]
                  //esta função será chamada quando o filho(child) do widget for pressionado
                    //Navigator.push(context,
                    //MaterialPageRoute(builder: (context) => HelpPage()),);
                    
                    Navigator.push(context, 
                      new MaterialPageRoute(builder: (context) => SpAboutPage(this.widget.user)));
                  },
                  child:Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      color: Colors.white, 
                      child: Container( //o container vai representar o nosso botão
                      width: 340.0,
                      height: 80.0,
                      alignment:  FractionalOffset(0.1, 0.5),
                      child: Text('Sobre',
                              style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                              ),
                  ))),
                  Container(
                    height: 3,
                  ),
                  GestureDetector( // um widget normal
                  onTap: () { // uma das propriedades do [GestureDetector]
                  //esta função será chamada quando o filho(child) do widget for pressionado
                  print(widget.auth);
                    Navigator.push(context,
                    MaterialPageRoute(builder: (context) => Chat(widget.auth,widget.user)),);
                  },
                  child:Card(
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(20.0),
                      ),
                      color: Colors.white, 
                      child: Container( //o container vai representar o nosso botão
                      width: 340.0,
                      height: 80.0,
                      alignment:  FractionalOffset(0.1, 0.5),
                      child: Text('Contato',
                              style: TextStyle(fontSize: 20, color: Color.fromRGBO(68, 68, 68, 1),fontWeight: FontWeight.bold,)
                              ),
                  ))),
          ],
        ),
      ),
     )]));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Perfil"),
        centerTitle: true,
      ),
      body: showProfile(),
    );
  }
}