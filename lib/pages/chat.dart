import 'package:flutter/material.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:intl/intl.dart';
import 'package:spot_service/models/User.dart';
import 'package:spot_service/services/authentication.dart';


class Chat extends StatefulWidget {
    
  final BaseAuth auth;
  final User user;

  Chat(this.auth, this.user);
  @override
  State<StatefulWidget> createState() => new _ChatState();
}

class _ChatState extends State<Chat> {

  var _firebaseRef = FirebaseDatabase().reference().child('chats');
  TextEditingController _txtCtrl = TextEditingController();
  String _email;
   @override
  void initState() {
    widget.auth.getCurrentUser().then((user) =>{_email=user.email});
  }

  sendMessage() {

    widget.auth.getCurrentUser().then((user) =>{
    _firebaseRef.push().set({
      "message": _txtCtrl.text,
      "timestamp": DateTime.now().millisecondsSinceEpoch,
      "email":user.email
    }),
    _txtCtrl.text = '',
    });
  }
  

  deleteMessage(key) {
    _firebaseRef.child(key).remove();
  }

  updateTimeStamp(key) {
    _firebaseRef
        .child(key)
        .update({"timestamp": DateTime.now().millisecondsSinceEpoch});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFEEEEEE),
        appBar: AppBar(
          centerTitle: true,
          title: Text("Chat"),
        ),
        body: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            children: <Widget>[
              Expanded(
                  child:
                StreamBuilder(
                  stream: _firebaseRef.onValue,
                  builder: (context, snap) {
                    
                    if (snap.hasData && !snap.hasError && snap.data.snapshot.value != null) {
                      
                      Map data = snap.data.snapshot.value;
                      List item = [];
                      //print(data);
                      data.forEach((index, data) => data["email"]==_email||data["email"]==widget.user.email||data["email"]=='resposta@resposta.com'? item.add({"key": index, ...data}):print("fix"));
                      //print(item);
                      return ListView.builder(
                              itemCount: item.length,
                              itemBuilder: (context, index) {
                                return Container(
                                  margin: new EdgeInsets.only(bottom: 10.0, right:item[index]["email"]=="fera@dobera.com"||item[index]["email"]=='resposta@resposta.com'? MediaQuery.of(context).size.width/4:10, left:item[index]["email"]=="fera@dobera.com"||item[index]["email"]=='resposta@resposta.com'? 10:MediaQuery.of(context).size.width/4),
                                   decoration: new BoxDecoration(
                                      color: item[index]["email"]=="fera@dobera.com"||item[index]["email"]=='resposta@resposta.com'?Colors.white:Colors.blue,
                                      borderRadius: BorderRadius.circular(50.0)
                                    ),
                                  child: ListTile(
                                    title: Text(item[index]['message']),
                                    trailing: Text(DateFormat("hh:mm:ss")
                                        .format(DateTime.fromMicrosecondsSinceEpoch(
                                            item[index]['timestamp'] * 1000))
                                        .toString()),
                                    onTap: () =>
                                        updateTimeStamp(item[index]['key']),
                                    onLongPress: () =>
                                        deleteMessage(item[index]['key']),
                                  ),
                                );
                              },
                            );
                    } else
                      return Center( child: Text("Entre em contato..."));
                  },
                )
              ),
              Container(
                  color:  Colors.white,
                  child: Row(children: <Widget>[
                    Expanded(child: TextField(controller: _txtCtrl)),
                    SizedBox(
                        width: 80,
                        height: 50,
                        child: OutlineButton(                          
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(50.0),
                            ),
                            child: Text("Enviar"), onPressed: () => sendMessage()))
                    
                  ]))
            ]));
  }
}