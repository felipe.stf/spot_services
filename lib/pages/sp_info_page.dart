import 'package:flutter/material.dart';
import 'package:spot_service/models/User.dart';

class SpInfoPage extends StatefulWidget {
  final User user;
  
  SpInfoPage(this.user);

  @override
  State<StatefulWidget> createState() => new _SpInfoPageState();
}

class _SpInfoPageState extends State<SpInfoPage> {

  Widget showProfile() {   
      return Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar (
          centerTitle: true ,
          title: Text('Informações Pessoais', style: TextStyle(color: Color(0XFF000000)),),
        backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(120),
          ),
        ),
        ),
        
      ),
      body: Column (
        children: <Widget>[
          Container (
            child: Column (
              children: <Widget>[
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("Nome", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 50,
                        child: Align (
                          alignment: Alignment.center,
                          child: Text(this.widget.user.name)
                        )
                      )
                   ]
                ),
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("E-mail", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 50,
                        child: Align (
                          alignment: Alignment.center,
                          child: Text(this.widget.user.email)
                        )
                      )
                   ]
                ),
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("Idade", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 50,
                        child: Align (
                          alignment: Alignment.center,
                          child: Text(this.widget.user.age.toString())
                        )
                      )
                   ]
                ),
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("Cidade", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 50,
                        child: Align (
                          alignment: Alignment.center,
                          child: Text(this.widget.user.city)
                        )
                      )
                   ]
                ),
            ],
            )
          )
        ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Spot Services'),
          centerTitle: true ,
        ),
        body: showProfile(),
        );
  }
}