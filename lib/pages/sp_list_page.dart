import 'package:flutter/material.dart';
import 'package:spot_service/models/User.dart';
import 'package:spot_service/services/authentication.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:spot_service/pages/sp_profile_page.dart';

class SpListPage extends StatefulWidget {
  SpListPage({Key key, this.title,this.auth, this.userId, this.logoutCallback}) : super(key: key);
 

  final String userId;
  final VoidCallback logoutCallback;
  final BaseAuth auth;
  final String title;

  @override
  State<StatefulWidget> createState() => new _SpListPageState();
}

class _SpListPageState extends State<SpListPage> {
  
  List<User> _notes = List<User>();
  List<User> _notesForDisplay = List<User>();

  Future<List<User>> fetchNotes() async {
    var url = 'http://www.json-generator.com/api/json/get/bUrDDvdVHC?indent=2';
    var response = await http.get(url);
    
    var notes = List<User>();
    
    if (response.statusCode == 200) {
      var notesJson = json.decode(response.body);
      for (var noteJson in notesJson) {
        notes.add(User.fromJson(noteJson));
      }
    }
    return notes;
  }

  @override
  void initState() {
    fetchNotes().then((value) {
      setState(() {
        _notes.addAll(value);
        _notesForDisplay = _notes;
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Busca'),
        centerTitle: true,
      ),
      body: ListView.builder(
        itemBuilder: (context, index) {
          return index == 0 ? _searchBar() : _listItem(index-1);
        },
        itemCount: _notesForDisplay.length+1,
      )
    );
  }

  _searchBar() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: TextField(
        decoration: InputDecoration(
          hintText: 'Buscar...'
        ),
        onChanged: (text) {
          text = text.toLowerCase();
          setState(() {
            _notesForDisplay = _notes.where((note) {
              var noteTitle = note.name.toLowerCase();
              return noteTitle.contains(text);
            }).toList();
          });
        },
      ),
    );
  }

  _listItem(index) {
    return new GestureDetector(
      onTap: () {
        Navigator.push(context, 
          new MaterialPageRoute(builder: (context) => SpProfilePage(_notesForDisplay[index],  widget.auth))
        );
      },
      child: new Card(
        child: Padding(
          padding: const EdgeInsets.only(top: 32.0, bottom: 32.0, left: 16.0, right: 16.0),
          child: Column(
            children: <Widget>[
              ListTile(
                title: Text(
                  _notesForDisplay[index].name,
                  style: TextStyle(
                    fontSize: 22,
                    fontWeight: FontWeight.bold
                  ),
                ),
                subtitle: Text(
                  _notesForDisplay[index].about.substring(0, 45) + "..."
                ),
                leading: CircleAvatar(backgroundImage: NetworkImage(_notesForDisplay[index].picture)),
              )
            ],
          )
        ), 
      ),
    ); 
  }
}