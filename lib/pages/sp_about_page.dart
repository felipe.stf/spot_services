import 'package:flutter/material.dart';
import 'package:spot_service/models/User.dart';
//import 'package:url_launcher/url_launcher.dart';

class SpAboutPage extends StatefulWidget {
  final User user;
  
  SpAboutPage(this.user);

  @override
  State<StatefulWidget> createState() => new _SpAboutPageState();
}

class _SpAboutPageState extends State<SpAboutPage> {

  var url1 = 'https://github.com/';

  Widget showProfile() {   
      return Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar (
          centerTitle: true ,
          title: Text('Informações Pessoais', style: TextStyle(color: Color(0XFF000000)),),
        backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(120),
          ),
        ),
        ),
        
      ),
      body: Column (
        children: <Widget>[
          Container (
            child: Column (
              children: <Widget>[
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("Resumo do perfil", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 150,
                        child: Align (
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(this.widget.user.about),
                          )
                        )
                      )
                   ]
                ),
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("Formações acadêmicas", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 85,
                        child: Align (
                          alignment: Alignment.center,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: new Column (
                              children: <Widget>[
                                new Text ("Universidade 1 - Bacharelado em Curso", textAlign: TextAlign.center),
                                new Text ("Universisdade 2 - Mestrado em Curso", textAlign: TextAlign.center),
                                new Text ("Curso extra - formado em Curso", textAlign: TextAlign.center),
                              ],
                            ),
                          ),
                        )
                      )
                   ]
                ),
                Column(
                   children: <Widget>[
                     Container(
                       margin: const EdgeInsets.only(top: 30.0, left: 30),
                       width: 350,
                       height: 20,
                       child: Align (alignment: Alignment.centerLeft,
                        child: Text("Links extras", style: TextStyle(fontWeight: FontWeight.bold))
                       )
                      ),
                      Container (
                        decoration: new BoxDecoration(color: Colors.white),
                        margin: const EdgeInsets.only(top: 10.0, left: 30),
                        width: 350,
                        height: 50,
                        child: GestureDetector(
                          child: RaisedButton (
                            child: const Text('Git Hub'),
                            onPressed: () {
                            //_launchInBrowser(url1);
                          }
                          ),
                        )
                      )
                   ]
                ),
            ],
            )
          )
        ],
        )
    );
  }
/* 
Future<void> _launchInBrowser(String url) async {
    if (await canLaunch(url)) {
      await launch(
        url,
        forceSafariVC: false,
        forceWebView: false,
        headers: <String, String>{'header_key': 'header_value'},
      );
    } else {
      throw 'Could not launch $url';
    }
} */

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Spot Services'),
          centerTitle: true ,
        ),
        body: showProfile(),
        );
  }
}