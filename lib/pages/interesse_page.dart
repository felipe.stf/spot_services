import 'package:flutter/material.dart';
import 'package:spot_service/models/Interesses.dart';
import 'dart:async';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/scheduler.dart' show timeDilation;


class InteressePage extends StatefulWidget {
  InteressePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  State<StatefulWidget> createState() => new _InteressePage();
}

class _InteressePage extends State<InteressePage> {
  //bool _isSelected = false;
  //var jsonData = [{'index':0,'name':"Eletricista",'check':true},{'index':1,'name':"Marcenaria",'check':false},{'index':2,'name':"Encanador",'check':false},{'index':3,'name':"Mecânico",'check':false}];
  var flag = true;
  List<Interesses> inter = [];
  Future<List<Interesses>> _getInters() async {

    //var data = await http.get("http://www.json-generator.com/api/json/get/caevBTcele?indent=2");

    //var jsonData = json.decode(data.body);
    //var jsonData = [{'index':0,'name':"Eletricista",'check':true},{'index':1,'name':"Marcenaria",'check':false},{'index':2,'name':"Encanador",'check':false},{'index':3,'name':"Mecânico",'check':false}];
    
    print("RECARREGA");
    

    

    //for(var u in jsonData){

      //Interesses user = Interesses(u["index"], u["name"],u['check']);

      //inter.add(user);

   // }
    if (flag){
      Interesses user = Interesses(0,"Eletricista",true);inter.add(user);
      user = Interesses(1, "Marcenaria",false);inter.add(user);
      user = Interesses(2, "Encanador",false);inter.add(user);
      user = Interesses(3, "Mecânico",false);inter.add(user);
      flag=false;
    }

    print(inter);

    return inter;

  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
      appBar: new AppBar(
        title: new Text("Interesses"),
        centerTitle: true,
      ),
      body: Container(
          child: FutureBuilder(
            future: _getInters(),
            builder: (BuildContext context, AsyncSnapshot snapshot){
              //print(snapshot.data);
              if(snapshot.data == null){
                return Container(
                  child: Center(
                    child: Text("Carregando...")
                  )
                );
              } else {
                return ListView.builder(

                  itemCount: snapshot.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    
                    return CheckboxListTile(
                      // leading: CircleAvatar(
                      //   backgroundImage: NetworkImage(
                      //     snapshot.data[index].picture
                      //   ),
                      // ),
                      title: Text(snapshot.data[index].name),
                      value: snapshot.data[index].getCheck(),
                      //value: false,
                      onChanged: (bool newvalue) {
                        setState(() {
                          //print(snapshot.data[index].check);
                          //timeDilation = value ? 2.0 : 1.0;
                          snapshot.data[index].setCheck(newvalue);
                        });
                      },
                      //secondary: const Icon(Icons.hourglass_empty),
                      // subtitle: Text(snapshot.data[index].about.substring(0, 45) + "..."),
                    );
                  },
                );
              }
            },
          ),
        ),
      );
  }
}