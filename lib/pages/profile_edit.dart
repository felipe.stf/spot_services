import 'package:flutter/material.dart';
import 'package:spot_service/services/authentication.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:spot_service/models/perfil.dart';
import 'dart:async';

class EditProfile extends StatefulWidget {
  EditProfile({Key key, this.auth, this.userId, this.logoutCallback})
      : super(key: key);

  final BaseAuth auth;
  final VoidCallback logoutCallback;
  final String userId;

  @override
  State<StatefulWidget> createState() => new _EditProfilePageState();
}

class _EditProfilePageState extends State<EditProfile> {
  List<Perfil> _todoList;
  String _name;
  String _email;
  String _born;
  String _cpfcnpj;
  String _city;
  String _errorMessage;

  final FirebaseDatabase _database = FirebaseDatabase.instance;
  final GlobalKey<FormState> formKey = GlobalKey<FormState>();
  StreamSubscription<Event> _onTodoAddedSubscription;
  StreamSubscription<Event> _onTodoChangedSubscription;
  Query _todoQuery;

  @override
  void initState() {
    super.initState();
    _todoList = new List();
    widget.auth.getCurrentUser().then((user) {
      print(_email);
        _email = user.email;
        _todoQuery = _database
        .reference()
        .child("perfil")
        .orderByChild("email")
        .equalTo(_email);
    _onTodoAddedSubscription = _todoQuery.onChildAdded.listen(onEntryAdded);
    });
    //_checkEmailVerification();
    }


  signOut() async {
    try {
      await widget.auth.signOut();
      widget.logoutCallback();
      Navigator.pop(context);
    } catch (e) {
      print(e);
    }
  }


  @override
  void dispose() {
    _onTodoAddedSubscription.cancel();
    super.dispose();
  }

  onEntryAdded(Event event) {
    setState(() {
      _todoList.add(Perfil.fromSnapshot(event.snapshot));
    });
  }

  Widget showProfile() {   
      return Scaffold(
        backgroundColor: const Color(0xFFEEEEEE),
      appBar: PreferredSize(
        preferredSize: Size.fromHeight(60.0),
        child: AppBar (
          centerTitle: true ,
          title: Text('Informações Pessoais', style: TextStyle(color: Color(0XFF000000)),),
        backgroundColor: Colors.white,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.vertical(
              bottom: Radius.circular(120),
          ),
        ),
        ),
        
      ),
      body: Column (
        children: <Widget>[
          Container (
            child: Column (
              children: <Widget>[
                Container(
                   margin: const EdgeInsets.only(top: 50.0, left: 30),
                   width: 350,
                   child: TextFormField(
                   decoration: InputDecoration(
                     filled: true,
                     fillColor: Colors.white,
                     border:OutlineInputBorder(
                       borderRadius: BorderRadius.circular(5)
                      ),
                     focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.blue),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Color(0XFFCCCCCC)),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                      ),
                     hintText: _todoList[0].name.isEmpty ? 'Nome' : _todoList[0].name
                    ),
                  ),
                ),
                Container(
                   margin: const EdgeInsets.only(top: 30.0, left: 30),
                   width: 350,
                   child: TextFormField(
                   decoration: InputDecoration(
                     filled: true,
                     fillColor: Colors.white,
                     border:OutlineInputBorder(
                       borderRadius: BorderRadius.circular(5)
                      ),
                     focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.blue),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Color(0XFFCCCCCC)),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                      ),
                     hintText: _todoList[0].email.isEmpty ? 'E-mail' : _todoList[0].email
                    ),
                  ),
                ),
                Container(
                   margin: const EdgeInsets.only(top: 30.0, left: 30),
                   width: 350,
                   child: TextFormField(
                   decoration: InputDecoration(
                     filled: true,
                     fillColor: Colors.white,
                     border:OutlineInputBorder(
                       borderRadius: BorderRadius.circular(5)
                     ),
                     focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.blue),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Color(0XFFCCCCCC)),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                      ),
                     hintText: _todoList[0].born.isEmpty ? 'Data de Nascimento' : _todoList[0].born
                    ),
                  ),
                ),
                Container(
                   margin: const EdgeInsets.only(top: 30.0, left: 30),
                   width: 350,
                   child: TextFormField(
                   decoration: InputDecoration(
                     filled: true,
                     fillColor: Colors.white,
                     border:OutlineInputBorder(
                       borderRadius: BorderRadius.circular(5)
                      ),
                     focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.blue),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Color(0XFFCCCCCC)),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                      ),
                     hintText: _todoList[0].cpfcnpj.isEmpty ? 'CPF/CNPJ' : _todoList[0].cpfcnpj
                    ),
                  ),
                ),
                Container(
                   margin: const EdgeInsets.only(top: 30.0, left: 30),
                   width: 350,
                   child: TextFormField(
                   decoration: InputDecoration(
                     filled: true,
                     fillColor: Colors.white,
                     border:OutlineInputBorder(
                       borderRadius: BorderRadius.circular(5)
                      ),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.blue),
                      ),
                      enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Color(0XFFCCCCCC)),
                      ),
                      errorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(5)),
                        borderSide: BorderSide(width: 1,color: Colors.red)
                      ),
                     hintText: _todoList[0].city.isEmpty ? 'cidade' : _todoList[0].city
                    ),
                  ),
                ),
                Container (
                  margin: const EdgeInsets.only(top: 30.0, left: 30),
                  width: 350,
                  height: 50,
                  child: RaisedButton(
                    onPressed: () {},
                    child: const Text('Realizar alterações', style: TextStyle(fontSize: 20, color: Colors.white)),
                    color: Colors.blue,
                  ),
                )
            ],
            )
          )
        ],
        )
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          title: new Text('Spot service'),
          centerTitle: true ,
          actions: <Widget>[
            new FlatButton(
                child: new Text('Logout',
                    style: new TextStyle(fontSize: 17.0, color: Colors.white)),
                onPressed: signOut)
          ],
        ),
        body: showProfile(),
        );
  }
}